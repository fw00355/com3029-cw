from pydantic import BaseModel
from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
import uvicorn
from model import predict


class RequestBody(BaseModel):
    text: str

app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")


@app.get("/", response_class=HTMLResponse)
def root():
    with open("static/index.html") as f:
        return f.read()


@app.post("/classify")
def classify(args: RequestBody):
  if len(args.text.split(" "))>1:
    prediction = predict(args.text)
  else:
    prediction = ["B-O"]
  output = ' '.join(prediction)
  #print(output)
  return output


if __name__ == "__main__":
  print("Running")
  uvicorn.run(app, host="0.0.0.0", port=5734)
