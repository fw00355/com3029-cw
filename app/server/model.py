import torchtext; torchtext.disable_torchtext_deprecation_warning()
import torch.nn as nn
import torch
import spacy
from logger import log

# Create LSTM
class LSTM(nn.Module):
  def __init__(self, input_dim, embedding_dim, hidden_dim, output_dim):
    super().__init__()
    self.embedding = nn.Embedding(input_dim, embedding_dim)
    self.lstm = nn.LSTM(embedding_dim, hidden_dim, batch_first=True)
    self.fc = nn.Linear(hidden_dim, output_dim)

  def forward(self, tokens):
    embedded = self.embedding(tokens)
    output, _ = self.lstm(embedded)
    return self.fc(output)

# Load vocabs
label_vocab = torch.load("label_vocab.pth")
token_vocab = torch.load("token_vocab.pth")

# Load model
model = LSTM(len(token_vocab), 100, 256, len(label_vocab))
model.load_state_dict(torch.load("lstm.pth"))
model.eval()

# Load tokenizer
tokenizer = spacy.load("en_core_web_sm")

# Predicts labels via LSTM
# Inputs a string sentence
# Outputs an array of labels
def predict(sentence):
  # Preprocess
  tokenized = tokenizer(sentence)
  tokens = [token.text for token in tokenized]
  tokens = [token_vocab[token] for token in tokens]
  tokens = torch.tensor(tokens).unsqueeze(0)
  
  # Predict
  with torch.no_grad():
    output = model(tokens)
    _, predictions_list = torch.max(output, 2)
    predicted = [label_vocab.get_itos()[label.item()] for label in predictions_list.squeeze()]

    # Log values
    log({ "input": sentence, "predictions": predicted})
    
    return predicted
