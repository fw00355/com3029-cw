import logging
import json

# Create logger formatter
class JSONFormatter(logging.Formatter):
  def format(self, record):
    log_data = {
      'timestamp': self.formatTime(record),
      'level': record.levelname,
      'message': record.getMessage(),
    }
    return json.dumps(log_data)

# Create logger
logger = logging.getLogger('json_logger')
logger.setLevel(logging.INFO)

# Create and link file handler
file_handler = logging.FileHandler('/app/logs/app.log')
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(JSONFormatter())
logger.addHandler(file_handler)

# Create log function
def log(message):
  logger.info(json.dumps(message))
