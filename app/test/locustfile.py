from locust import HttpUser, between, task
from sentence import generate_sentence

# Create locust class for the classification endpoint
class ClassUser(HttpUser):
  # Wait time of 0.5s - 2s
  wait_time = between(0.5, 2)

  @task
  def classify(self):
    # POST to classify with a randomly generated sentence
    self.client.post("/classify", json={"text": generate_sentence()})
