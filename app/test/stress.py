import subprocess
import os
import csv
server_address = os.getenv('SERVER_ADDRESS', 'localhost')
# Create list of locust statistics files
files = [
  'stress/stress_exceptions.csv',
  'stress/stress_failures.csv',
  'stress/stress_stats_history.csv',
  'stress/stress_stats.csv'
]

# Remove these files if they exist
for path in files:
  if os.path.exists(path):
    os.remove(path)
run_time = 10
users = 100
print(f"Performing {run_time}s of stress testing, with {users} users.")
# Perform the locust testing
command = f"python -m locust -f test/locustfile.py --csv stress/stress --host=http://{server_address}:5734 --users 100 --spawn-rate 4 --headless --run-time {run_time}s"
subprocess.run(command, shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

# Output the result statistics
with open('stress/stress_stats.csv', 'r') as file:
  reader = csv.DictReader(file)  
  for row in reader:
    if row['Name'] == '/classify':
      print("Endpoint Name:", row['Name'])
      print("Request Count:", int(row['Request Count']))
      print("Failure Count:", int(row['Failure Count']))
      print("Average Response Time: {:.3f} ms".format(float(row['Average Response Time'])))
      print("Min Response Time: {:.3f} ms".format(float(row['Min Response Time'])))
      print("Max Response Time: {:.3f} ms".format(float(row['Max Response Time'])))
      print("Average Content Size: {:.3f} bytes".format(float(row['Average Content Size'])))
      break
