import os
import requests
import time

def make_request(sentence, server_address):
    try:
        response = requests.post(
            f"http://{server_address}:5734/classify",
            json={"text": sentence}
        )
        if response.status_code == 200:
            print("Predicted:", response.json())
        else:
            print("Error:", response.text)
    except Exception as e:
        print("An unexpected error has occurred, is the server online? ", e)

sentence = "This is a test sentence."
print("Sentence:", sentence)

server_address = os.getenv('SERVER_ADDRESS', 'localhost')

max_retries = 5
for attempt in range(max_retries):
    try:
        make_request(sentence, server_address)
        break
    except Exception as e:
        print(f"Attempt {attempt + 1} failed: {e}")
        time.sleep(5)
else:
    print("Failed to connect after several attempts.")
