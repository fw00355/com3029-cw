import random

# Create list of custom filler words
custom = ['(Of course)', '(not)', 'IT', 'CS', 'Hello', 'et', 'al', 'vitae', 'sed', 'e.g', 'i.e', 'e.g.', 'i.e.', 'etc', 'Dr', 'Mr', 'Mrs']

# Create list of sentence structures
structures = [
  "The {} was found {} in the experiment.",
  "According to the {}, {} is the most common cause of {}.",
  "The {} stated that {} is the key factor in {}.",
  "The {} concluded that {} is not related to {}.",
  "It is important to consider {} when discussing {}."
]

# Generates a sentences based on the structures and the custom words
def generate_sentence():
  structure = random.choice(structures)
  words = random.choices(custom, k=structure.count('{}'))
  sentence = structure.format(*words)
  return sentence
