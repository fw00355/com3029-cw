import torchtext; torchtext.disable_torchtext_deprecation_warning()
import torch
from datasets import load_dataset
from torchtext.data.functional import to_map_style_dataset
from torchtext.vocab import build_vocab_from_iterator, vocab
from collections import OrderedDict
from torch.utils.data import Dataset, DataLoader
from torch.nn.utils.rnn import pad_sequence
from pandas import DataFrame
from sklearn.metrics import confusion_matrix, f1_score
import torch.optim as optim
import torch.nn as nn

# Inits
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(DEVICE)
torch.manual_seed(1234)
torch.backends.cudnn.deterministic = True

# Load dataset
raw_dataset = load_dataset("surrey-nlp/PLOD-CW")

# Convert raw dataset into maps
train_dataset = to_map_style_dataset(raw_dataset['train'])
validation_dataset = to_map_style_dataset(raw_dataset['validation'])
test_dataset = to_map_style_dataset(raw_dataset['test'])

# Create vocabs
def token_vocab_iterator(data):
  for item in data:
    yield item['tokens']

token_vocab = build_vocab_from_iterator(token_vocab_iterator(train_dataset), specials=("<unk>", "<pad>"), max_tokens=50_000)
token_vocab.set_default_index(token_vocab["<unk>"])

label_vocab = vocab(OrderedDict([("<pad>", 1), ("B-O", 1), ("B-AC", 1), ("B-LF", 1), ("I-LF", 1)]))

# Create LSTM
class LSTM(nn.Module):
  def __init__(self, input_dim, embedding_dim, hidden_dim, output_dim):
    super().__init__()
    self.embedding = nn.Embedding(input_dim, embedding_dim)
    self.lstm = nn.LSTM(embedding_dim, hidden_dim, batch_first=True)
    self.fc = nn.Linear(hidden_dim, output_dim)

  def forward(self, tokens):
    embedded = self.embedding(tokens)
    output, _ = self.lstm(embedded)
    return self.fc(output)

# Create dataloader functions
class SeqClassDataset(Dataset):
  def __init__(self, data, token_vocab, label_vocab):
    self.data = data
    self.token_vocab = token_vocab
    self.label_vocab = label_vocab

  def __len__(self):
    return len(self.data)

  def __getitem__(self, idx):
    tokens = self.data[idx]['tokens']
    ner_tags = self.data[idx]['ner_tags']
    token_numerical = [self.token_vocab[token] for token in tokens]
    label_numerical = [self.label_vocab[label] for label in ner_tags]

    return torch.tensor(token_numerical, dtype=torch.long), torch.tensor(label_numerical, dtype=torch.long)

def collate_batch(batch):
  labels, tokens = zip(*batch)

  tokens_padded = pad_sequence(tokens, batch_first=True, padding_value=token_vocab['<pad>'])
  labels_padded = pad_sequence(labels, batch_first=True, padding_value=label_vocab['<pad>'])

  return tokens_padded, labels_padded

def create_dataloader(data):
  return DataLoader(data, batch_size=64, shuffle=True, collate_fn=collate_batch)

# Create training helper functions
def train(model, iterator, optimizer, criterion):
  model = model.to(DEVICE)
  epoch_loss = 0
  model.train()

  for batch in iterator:
    labels, tokens = batch
    labels = labels.to(DEVICE)
    tokens = tokens.to(DEVICE)
    optimizer.zero_grad()

    predictions = model(tokens)
    predictions = predictions.view(-1, predictions.shape[-1])
    labels = labels.view(-1)

    loss = criterion(predictions, labels)
    loss.backward()
    optimizer.step()
    epoch_loss += loss.item()

  return epoch_loss / len(iterator)

def evaluate(model, iterator, criterion):
  model = model.to(DEVICE)
  epoch_loss = 0
  model.eval()

  with torch.no_grad():
    for batch in iterator:
      labels, tokens = batch
      labels = labels.to(DEVICE)
      tokens = tokens.to(DEVICE)
      predictions = model(tokens)
      predictions = predictions.view(-1, predictions.shape[-1])
      labels = labels.view(-1)
      labels = labels - 1

      loss = criterion(predictions, labels)
      epoch_loss += loss.item()

  return epoch_loss / len(iterator)

# Create predict and metrics functions
def predict(model, iterator):
  model = model.to(DEVICE)
  model.eval()

  with torch.no_grad():
    for batch in iterator:
      labels, tokens = batch
      labels = labels.to(DEVICE)
      tokens = tokens.to(DEVICE)
      predictions_list = model(tokens).argmax(dim=2).tolist()
      labels_list = labels.tolist()

      predicted = [[label_vocab.get_itos()[index] for index in sentence] for sentence in predictions_list]
      actual = [[label_vocab.get_itos()[index] for index in sentence] for sentence in labels_list]

      return predicted, actual

def calculate_metrics(predicted, actual):
  predicted_flat = [tag for sublist in predicted for tag in sublist]
  actual_flat = [tag for sublist in actual for tag in sublist]

  labels = ['B-O', 'B-AC', 'B-LF', 'I-LF']
  cm = DataFrame(confusion_matrix(actual_flat, predicted_flat, labels=labels), index=labels, columns=labels)
  f1 = f1_score(actual_flat, predicted_flat, average='weighted', labels=labels)

  return cm, f1

# Train LSTM model
train_dataloader = create_dataloader(SeqClassDataset(train_dataset, token_vocab, label_vocab))
validation_dataloader = create_dataloader(SeqClassDataset(validation_dataset, token_vocab, label_vocab))
test_dataloader = create_dataloader(SeqClassDataset(test_dataset, token_vocab, label_vocab))

model = LSTM(len(token_vocab), 100, 256, len(label_vocab))
optimizer = optim.Adam(model.parameters())
criterion = nn.CrossEntropyLoss()

for epoch in range(50):
  train_loss = train(model, train_dataloader, optimizer, criterion)
  validation_loss = evaluate(model, validation_dataloader, criterion)
  print("epoch {:<2d} train_loss {:.3f} validation_loss {:.3f}".format(epoch, train_loss, validation_loss))

import os
print(os.getcwd())
torch.save(model.state_dict(), 'server/lstm.pth')
torch.save(token_vocab, 'server/token_vocab.pth')
torch.save(label_vocab, 'server/label_vocab.pth')
print()

# Predictions for LSTM model
predicted, actual = predict(model, test_dataloader)

cm, f1 = calculate_metrics(predicted, actual)

print("f1", f1)
print()
print(cm)
print()
